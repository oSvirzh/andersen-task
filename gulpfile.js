var gulp          = require('gulp'),
    less          = require('gulp-less'),
    pug           = require('gulp-pug'),
    concat        = require('gulp-concat'),
    uglify        = require("gulp-uglify"),
    autoprefixer  = require('gulp-autoprefixer'),
    path          = require('path'),

    runSequence   = require('run-sequence'),
    browserSync   = require('browser-sync').create(),

    paths   = {
        js          : './app/js/',
        images      : './app/images/',
        fonts       : './app/fonts/',
        less        : './app/less/',
        pug         : './app/templates/',
        dest        : {
            root        : './public/'
        }
    },
    sources = {
        jsSrc   : function() {
            var scripts = ['node_modules/jquery/dist/jquery.min.js',
                'node_modules/slick-carousel/slick/slick.min.js',
                paths.js + 'main.js'
            ];
            return gulp.src(scripts)
        },
        imgSrc      : function() { return gulp.src([
            paths.images + '**/*.png',
            paths.images + '**/*.jpg',
            paths.images + '**/*.gif',
            paths.images + '**/*.jpeg',
            paths.images + '**/*.svg',
            paths.images + '**/*.ico'
        ])},
        fontsSrc      : function() { return gulp.src([
            paths.fonts + '**/*.woff',
            paths.fonts + '**/*.ttf',
            paths.fonts + '**/*.eot'
        ])},
        lessSrc     : function() { return gulp.src([paths.less + 'main.less']) },
        pugSrc     : function() { return gulp.src([
            paths.pug + 'index.pug'
        ])}
    };

gulp.task('js', function() {

    sources.jsSrc()
        .pipe(concat('main.js'))
        .on('error', console.log)
        .pipe(gulp.dest(paths.dest.root + 'js'));
});

gulp.task('less', function() {
    gulp.src(['node_modules/slick-carousel/slick/ajax-loader.gif'])
        .pipe(gulp.dest('./public/css/'));

    gulp.src(['node_modules/slick-carousel/slick/fonts/*'])
        .pipe(gulp.dest('./public/css/fonts/'));

    sources.lessSrc()
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(autoprefixer())
        .on('error', console.log)
        .pipe(gulp.dest(paths.dest.root + 'css'));
});

gulp.task('pug', function() {

    var locals = {};

    sources.pugSrc()
        .pipe(pug({ locals: locals }))
        .on('error', console.log)
        .pipe(gulp.dest(paths.dest.root));
});

gulp.task('images', function() {
    sources.imgSrc()
        .on('error', console.log)
        .pipe(gulp.dest(paths.dest.root + 'images'));
});

gulp.task('fonts', function() {
    sources.fontsSrc()
        .on('error', console.log)
        .pipe(gulp.dest(paths.dest.root + 'fonts'));
});

gulp.task('server',     function() {
    browserSync.init({
        server: {
            baseDir: paths.dest.root
        },
        files: [
            paths.app + '**/*.*',
            paths.dest.root + '**/*.*'
        ],
        port: 8000,
        ui: { port: 8001 }
    });
});

gulp.task('compile', ['pug', 'less', 'js', 'images', 'fonts']);

gulp.task('default',    function() {
    runSequence('watch', 'server');
});

gulp.task('watch',  ['compile'], function () {
    gulp.watch([paths.images    + '**/*.*'],    ['img'],   browserSync.reload);
    gulp.watch([paths.fonts     + '**/*.*'],    ['fonts'], browserSync.reload);
    gulp.watch([paths.pug       + '**/*.pug'],  ['pug'],   browserSync.reload);
    gulp.watch([paths.less      + '**/*.less'], ['less'],  browserSync.reload);
    gulp.watch([paths.js        + '**/*.js'],   ['js'],    browserSync.reload);
});