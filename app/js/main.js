function langSwitcherOpen() {
    document.getElementsByClassName("header__lang")[0].classList.toggle("header__lang_opened");
}

window.onclick = function (event) {
    if (event.target.matches('.js-show-lang')) {
        langSwitcherOpen();
    } else {
        var dropdowns = document.getElementsByClassName("header__lang");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('header__lang_opened')) {
                openDropdown.classList.remove('header__lang_opened');
            }
        }
    }
}

$('.about__slides').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: false,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 720,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true
            }
        }
    ]
});
